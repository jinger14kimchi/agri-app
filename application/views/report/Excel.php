<html>
<head>
    <title>Export Data to Excel in Codeigniter using PHPExcel</title>
    
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
</head>
<body>
 <div class="container box">

  <br />
  <div class="table-responsive">
   <table class="table table-bordered">
    <tr>
     <th>Product ID</th>
     <th>Product Name</th>
     <th>Description</th>
     <th>Date Created</th>
    </tr>
    <?php
    foreach($commodities as $row)
    {
     echo '
     <tr>
      <td>'.$row->product_id.'</td>
      <td>'.$row->product_name.'</td>
      <td>'.$row->description.'</td>
      <td>'.$row->date_created.'</td>
     </tr>
     ';
    }
    ?>
   </table>
   <div align="center">
    <form method="post" action="<?php echo base_url(); ?>pages/excel_export/action">
     <input type="submit" name="export" class="btn btn-success" value="Export" />
    </form>
   </div>
   <br />
   <br />
  </div>
 </div>
</body>
</html>
