
<!-- Modal Add Category -->

<div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Category</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/addcategory'); ?>" method="POST">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Category Name</label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" name="category_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea type="email" class="form-control" name="category_description" placeholder="Add addtional information about this commodity(optional)"></textarea>
                        </div>
                    </div>
                </div>
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div> 
            <div class="clearfix"></div>
      </form>
        </div>
      </div>
    </div>

<!-- End Modal Add Category-->