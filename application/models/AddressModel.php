<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AddressModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function insertAddress($address) {
		$this->db->insert('address', $address);
		return $this->db->insert_id();
	}
}