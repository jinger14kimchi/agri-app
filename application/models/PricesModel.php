<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PricesModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
    }

    public function getLatestReport() {
        $sql = "SELECT monitoring_date FROM monitoring ORDER BY monitoring_id DESC LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->result();
        if ($result) {
            $latest = date("Y-m-d",strtotime($result[0]->monitoring_date)); 
            return $latest;
        }
        return false;
    }

    public function getReportsByProduct($id) {
        $sql = "SELECT * FROM summary_prev_price WHERE product_id = '". $id . "' ORDER BY monitoring_date DESC LIMIT 10";
        $this->db->where('product_id', $id);
        $query = $this->db->get('summary_prev_price');
        return $query->result();
    }

    public function getSummaryReports($date) {
        $this->db->where('monitoring_date', $date);
        $query = $this->db->get('summary_prev_price');
        return $query->result();
    }

    public function getPrevailingPriceReports($date = null) {
        $sql = "SELECT mon.product_id, cat.category_id,
					cat.category_name, prod.product_name, mon.unit, mon.market_id, 
                    mar.market_name, ROUND(AVG(mon.price)) as prevprice,
					date(mon.monitoring_date) as monitoring_date
                FROM monitoring mon 
                LEFT JOIN product prod 
					ON mon.product_id = prod.product_id 
				LEFT JOIN market mar 
					ON mon.market_id = mar.market_id 
				LEFT JOIN category cat
					ON cat.category_id = prod.category_id
				GROUP BY mon.product_id, mon.unit, mon.market_id, date(mon.monitoring_date) 
				ORDER BY monitoring_date DESC";
		$query = $this->db->query($sql);
        $result = $query->result();
        return $result;
	}
	
    
    public function getRawPricesReportIndividual($market_id) {	
        $this->db->select('monitoring.monitoring_id, product.product_name, product.product_id, market.market_id, 
                            category.category_id, category.category_name, monitoring.unit, monitoring.monitoring_date,
                            monitoring.price, market.market_name, monitoring.sources');
		$this->db->join('market', 'market.market_id = monitoring.market_id', 'left');
		$this->db->join('product', 'product.product_id = monitoring.product_id', 'left'); 
		$this->db->join('category', 'category.category_id = product.category_id', 'left'); 
        $this->db->where('monitoring.product_id', $product_id);
        $this->db->where('DATE(monitoring_date)', $monitoring_date);
        $this->db->where('monitoring.market_id', $market_id);
        $query = $this->db->get('monitoring');
        $data = $query->result();
        
        return $data;
    }

    
    public function getRawPricesByMarket($market_id) {	
        $this->db->select('monitoring.monitoring_id, product.product_name, product.product_id, market.market_id, 
                            category.category_id, category.category_name, monitoring.unit, monitoring.monitoring_date,
                            monitoring.price, market.market_name, monitoring.sources');
		$this->db->join('market', 'market.market_id = monitoring.market_id', 'left');
		$this->db->join('product', 'product.product_id = monitoring.product_id', 'left'); 
		$this->db->join('category', 'category.category_id = product.category_id', 'left'); 
        $this->db->where('monitoring.market_id', $market_id);
        $query = $this->db->get('monitoring');
        $data = $query->result();
        
        return $data;
    }


    public function getRawPricesByProduct($product_id) {	
        $this->db->select('monitoring.monitoring_id, product.product_name, product.product_id, market.market_id, 
                            category.category_id, category.category_name, monitoring.unit, monitoring.monitoring_date,
                            monitoring.price, market.market_name, monitoring.sources');
		$this->db->join('market', 'market.market_id = monitoring.market_id', 'left');
		$this->db->join('product', 'product.product_id = monitoring.product_id', 'left'); 
		$this->db->join('category', 'category.category_id = product.category_id', 'left');                  
        $this->db->where('monitoring.product_id', $product_id);
        $query = $this->db->get('monitoring');
        $data = $query->result();
        
        return $data;
    }

    public function getRawPricesByDate($monitoring_date) {	
        $this->db->select('monitoring.monitoring_id, product.product_name, product.product_id, market.market_id, 
                            category.category_id, category.category_name, monitoring.unit, monitoring.monitoring_date,
                            monitoring.price, market.market_name, monitoring.sources');
		$this->db->join('market', 'market.market_id = monitoring.market_id', 'left');
		$this->db->join('product', 'product.product_id = monitoring.product_id', 'left'); 
		$this->db->join('category', 'category.category_id = product.category_id', 'left');   
        $this->db->where('DATE(monitoring_date)', $monitoring_date);
        $query = $this->db->get('monitoring');
        $data = $query->result();
        
        return $data;
    }

    public function getSummaryReport($data) {
        $res = [];
        foreach ($data as $d) {
            $single = $this->summarize($d);
            array_push($res, $single);
        }

    }
    
    public function summarize($data) {

    }

    public function getPrevailingPrice($data) {
        $prices = 0;
        $count = 0;
        foreach ($data as $item) {
            $prices += $item->price;
            $count ++;
        }
        return (round($prices/$count));
    }

    public function getSources($data) {
        $sources = [];
        foreach ($data as $item) {
            array_push($sources, $item->source);
        }
        return array_unique($sources);
    }

    public function insertMonitoring($data) {
        $this->db->insert('monitoring', $data);
		return $this->db->insert_id();

    }
    
}