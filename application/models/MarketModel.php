<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MarketModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getMarkets() {		
		$this->db->select('*');
		$this->db->from('market');
		$query = $this->db->get();
		return $query->result();
	}

	public function getMarket($id) {
		$this->db->select('*');
		$this->db->from('market');
		$this->db->where('market_id', $id); 
		$query = $this->db->get();
		return $query->result();

	}
}