<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AgentModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getAgents() {		
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('address', 'address.address_id = user.address_id');

		$this->db->where('user_type', 'agent'); 
		$query = $this->db->get();
		return $query->result();
	}

	public function getAgent($id) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('address', 'address.address_id = user.address_id');
		$this->db->where('user_id', $id);
		$this->db->where('user_type', 'agent'); 
		$query = $this->db->get();
		return $query->result();
	}
}