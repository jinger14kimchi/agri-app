<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getCategories() {	
		$query = $this->db->get('category');
		return $query->result();
	}


	public function getCategoryById($id) {	
		$this->db->where('category_id', $id); 
		$query = $this->db->get('category');
		return $query->result();
	}

	public function addNewCategory($category_info) {
		$this->db->insert('category', $category_info);
		return $this->db->insert_id();
	}

	public function a() {
		
	}
}