<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getUsers() {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('address', 'address.address_id = user.address_id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	public function getUser($id) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->join('address', 'address.address_id = user.address_id', 'left');
		$this->db->where('user_id', $id); 
		$query = $this->db->get();
		return $query->result();		
	}

	public function getUserByType($user_type) {
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_type', $user_type); 
		$query = $this->db->get();
		return $query->result();		
	}

	public function addUser($info) {
		$this->db->insert('user', $info);
		return $this->db->insert_id();
	}

	public function updateUser($info) {
		if ($info['password'] != null) {
			$info['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
		}
		$info['date_updated'] = date("Y-m-d H:i:s");
	    $this->db->where('user_id', $info['user_id']);
	    $this->db->update('user', $info);
		$affected = $this->db->affected_rows();
		if ($affected > 0) {
			return true;
		}
	}

	public function validate($username, $password) {
		$this->db->where('username', $username); 		
		$query = $this->db->get('user');

        if ($query->num_rows() == 1) {
			$row = $query->row();			
			$auth = password_verify($password, $row->password);	
			if ($auth) {
				$data = array(
						'user_id' => $row->user_id,
						'fname' => $row->fname,
						'lname' => $row->lname,
						'username' => $row->username,
						'user_type' => $row->user_type,
						'validated' => true
						);
				$this->session->set_userdata($data);
				return $row;
			}
        }
		return false;	

	}
}