<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CheckAccessModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

    public function activeSession() {
		if ($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){       
			return true;
		}
		return false;
    }
}