<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CommodityModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getMarkets() {		
		$this->db->select('*');
		$this->db->from('market');
		$this->db->join('address', 'address.address_id = market.address_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getMarket($id) {
		$this->db->select('*');
		$this->db->from('market');
		$this->db->join('address', 'address.address_id = market.address_id');
		$this->db->where('market_id', $id); 
		$query = $this->db->get();
		return $query->result();
	}
}