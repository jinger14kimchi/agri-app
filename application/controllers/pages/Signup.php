<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
	public function __construct(){
			parent::__construct();
			$this->load->model('PasswordModel');
	}
	public function index()
	{
			$this->load->helper('url');

		$this->load->view('pages/signuppage');

	}


	public function hash_get(){
		$password= $this->input->get('password');
		$username= $this->input->get('username');
		$auth =$this->PasswordModel->getUserHash($username, $password);
		if ($auth == true ){
			$this ->response ([
				'hashed' => $auth,
				'message' => "Verified Account"], 200);
		}
		else{
			$this->response("Sayop ang pass", 200);
		}
	}
}
	// // Procedures
	// public function create_user(){
	// //	if($this->check_access()){

	// 		$hashedpsw = $this->input->post(); // $_POST -> array
	// 		$result = $this->PasswordModel->addUserHash($hashedpsw);

	// 		if($result == true){
	// 			$data['msg'] =  "Successfully Added!";
	// 		}else{
	// 			$data['msg'] =  "Failed to add";
	// 		}

	// 		// $data['content'] = 'pages/';
	// 		// $data['recordset'] = $this->account->read();
	// 		// $this->load->view('admin/admin', $data);
	// 	}
	//}


	// public function check_access(){
	// 	if($this->session->userdata('loggedin') == true && $this->session->userdata('accesslevel') == 0){
	// 		return true;
	// 	}else{
	// 		$data['msg'] = "Access denied";
 //            $this->load->view('portal', $data);
	// 	}
	// }
