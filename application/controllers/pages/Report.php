<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('UsersModel', 'usermodel');

		$this->load->library('Pdf');
		// $this->load->view('view_file');
	}


	public function generate(){
		$data['recordset'] = $this->usermodel->read();
		$data['docTitle'] = "Product Report";
	//	$data['pageTitle'] = "Student List of Accounts";

		$this->load->view('report/listOfCommodities', $data);
	}

	public function generate2(){
		$this->load->model('PricesModel');

		$data['reports'] = $this->PricesModel->getReports();
		$data['docTitle'] = "Price Report";
	//	$data['pageTitle'] = "Student List of Accounts";

		$this->load->view('report/priceReport', $data);
	}




}
