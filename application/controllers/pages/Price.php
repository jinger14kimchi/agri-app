<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller {
	
    function __construct() {
        parent::__construct();
        $this->load->model('PricesModel', 'pricesmodel');
    }

	public function reports() {
		$data['reports'] = json_decode( file_get_contents(base_url('/api/prices/reports')));
		$data['products'] = json_decode(file_get_contents(base_url('/api/product/product')));
		$data['categories'] = json_decode(file_get_contents(base_url('/api/category/category')));
		$this->load->view('pages/monitoring', $data);
	}

	public function reportsKspm() {

		$res = json_decode(
			file_get_contents(base_url('/api/prices/prices'))
		);
		$data['report'] = $res->result;
		// print_r($data);
		// return;
		$this->load->view('pages/monitoring', $data);
	}

}
