<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index(){
        if($this->check_access()){        
        	$this->load->view('pages/dashboard');
		}
	}
	
	public function check_access(){
		if ($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){
			return true;
		} 
		else{
			$data['msg'] = "Access denied";
            $this->load->view('pages/loginpage', $data);
		}
	}
}
