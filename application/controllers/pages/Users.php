<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('UsersModel', 'usersmodel');
    }

	function index() {
		$this->getAllAgents();
	}

	public function updateUser() {
		$info = $this->input->post();
		if ($this->usersmodel->updateUser($info)) {
			redirect('pages/commodities');
		}

	}
}
	
