<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductHistory extends CI_Controller {
	
    function __construct() {
        parent::__construct();
        $this->load->model('PricesModel', 'pricesmodel');
    }

	public function history() {
		$id = $this->input->get('id');
		if ($id) {
			$data['product'] = json_decode(file_get_contents(base_url('/api/product/product?id=') . $id));
			$data['category'] = json_decode(file_get_contents(base_url('/api/category/category?id=') . $id));
			$data['reports'] = json_decode(file_get_contents(base_url('/api/prices/history?id=') . $id));
			if($this->check_access()){
				$this->load->view('pages/product_history', $data);
			}
		}
		else {
			$this->load->view('template/not_found');
		}
	}



	public function check_access() {
		if ($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){
			return true;
		}
		else {
			$data['msg'] = "Access denied";
            $this->load->view('pages/loginpage', $data);
		}
	}

}