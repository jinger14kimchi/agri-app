<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_export extends CI_Controller {
 
 // function index()
 // {
 //  $this->load->model('UsersModel', 'usermodel');
 //  $data["commodities"] = $this->usermodel->read();
 //  $this->load->view("report/excel", $data);
 // }

 function action()
 {
  $this->load->model('UsersModel', 'usermodel');
  $this->load->library("excel");
  $object = new PHPExcel();

  $object->setActiveSheetIndex(0);

  $table_columns = array("PRODUCT ID", "PRODUCT NAME", "DESCRIPTION", "DATE CREATED");

  $column = 0;

  foreach($table_columns as $field)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
   $column++;
  }

  $commodities = $this->usermodel->read();

  $excel_row = 2;

  foreach($commodities as $row)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->product_id);
   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->product_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->description);
   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->date_created);

  $object->getActiveSheet()->getColumnDimension('A')->setWidth('15');
  $object->getActiveSheet()->getColumnDimension('B')->setWidth('25');
  $object->getActiveSheet()->getColumnDimension('C')->setWidth('40');
  $object->getActiveSheet()->getColumnDimension('D')->setWidth('25');
  $object->getActiveSheet()->getStyle('A')->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
   $object->getActiveSheet()->getStyle('A2')->getAlignment()
->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $object->getActiveSheet()->getStyle('A1:D1')->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
  $object->getActiveSheet()->getStyle('D')->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
   $object->getActiveSheet()->getStyle('D2')->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
   $excel_row++;
  }


  

  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="List of Commodities.xls"');
  header('Cache-Control: max-age=0');
  ob_end_clean();
  ob_start();
  $object_writer->save('php://output');
 }

 
 




 function action2()
 {
  $this->load->model('PricesModel', 'pricemodel');
  $this->load->library("excel");
  $object = new PHPExcel();

  $object->setActiveSheetIndex(0);

  $table_columns = array("CATEGORY", "COMMODITY", "UNIT", "COGON PREVAILING PRICE" , "CAREMEN PREVAILING PRICE", "SOURCE");

  $column = 0;

  foreach($table_columns as $field)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
   $column++;
  }

  $reports = $this->pricemodel->getReports();

  $excel_row = 2;

  foreach($reports as $row)
  {
   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->category_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->product_name);
   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->unit);
   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->cogon_prev_price);
$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->carmen_prev_price);

  $object->getActiveSheet()->getColumnDimension('A')->setWidth('20');
  $object->getActiveSheet()->getColumnDimension('B')->setWidth('25');
  $object->getActiveSheet()->getColumnDimension('C')->setWidth('10');
  $object->getActiveSheet()->getColumnDimension('D')->setWidth('28');
    $object->getActiveSheet()->getColumnDimension('E')->setWidth('28');
    $object->getActiveSheet()->getColumnDimension('F')->setWidth('20');
  

  $object->getActiveSheet()->getStyle('A1:F1')->getAlignment()
  ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

   $excel_row++;
  }


  

  $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Price Monitoring Report.xls"');
  header('Cache-Control: max-age=0');
  ob_end_clean();
  ob_start();
  $object_writer->save('php://output');
 }


 
 
}