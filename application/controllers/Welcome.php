<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->view('pages/landing');
	}

	public function logout() {
        $this->session->sess_destroy();
		redirect('welcome');
    }
}

