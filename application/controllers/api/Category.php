<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Category extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('CategoryModel', 'categorymodel');
    }


    public function category_get() {
        $id = $this->get('id');

        if ($id === NULL) {
            $categories = $this->categorymodel->getCategories();
            if ($categories) {
                $this->response($categories, REST_Controller::HTTP_OK);
            }

        }
        else {        	
            $category = $this->categorymodel->getCategoryById($id);
            $this->response($category, 200);
        }
    }

    public function category_post() {
        $id = $this->post('id');
        
        if ($id === NULL) {                
            $address = array(
                'street' => $this->post('street'),
                'barangay' => $this->post('barangay'),
                'city' => $this->post('city'),
                'zipcode' => $this->post('street')

            );
            $address_id = $this->addressmodel->insertAddress($address);

            if ($address_id) {                
                $info = array(
                    'fname' => $this->post('fname'),
                    'lname' => $this->post('lname'),
                    'birthdate' => $this->post('birthdate'),
                    'gender' => $this->post('gender'),
                    'username' => $this->post('username'),
                    'password' => $this->post('password'),
                    'user_type' => $this->post('user_type'),
                    'address_id' => $address_id
                );
                $last_id = $this->usersmodel->addUser($info);

                if ($last_id) {                    
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Successfully added user.'
                    ], REST_Controller::HTTP_OK);
                }
                else {
                    return 'Please check you information and try again';
                }
            }
            else {
                return "There's an error in the address";
            }
        }
        else {
            var_dump("THERES NOTING");
            $user = $this->usersmodel->getUser($id);
            $this->response($user, 200);
        }
    }
        
}