<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Prices extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('PricesModel', 'pricesmodel');
        $this->load->model('ReportingModel', 'reportingmodel');
    }

    public function history_get() {
        $id = $this->input->get('id');
        $history = $this->reportingmodel->getMonitoringEntryProd($id);
        if ($history) {
            $this->response($history, 200);
        } 
        else {
            $this->response('No history found', 404);
        }
    }

    public function market_get() {
        $market_id = $this->input->get('market_id');
        $reports = $this->pricesmodel->getRawPricesByMarket($market_id);
        if ($reports) {            
            $this->response($reports, REST_Controller::HTTP_OK);
        }
    }   

    public function product_get() {
        $product_id = $this->input->get('id');
        $reports = $this->pricesmodel->getReportsByProduct($product_id);
        if ($reports) {
            $this->response($reports, REST_Controller::HTTP_OK);
        }
        else {
            $this->response("Product not found", 404);
        }
    }

    public function date_get() {
        $monitoring_date = $this->input->get('monitoring_date');
        $reports = $this->pricesmodel->getRawPricesByDate($monitoring_date);
        if ($reports) {
            $this->response($reports, REST_Controller::HTTP_OK);
        }
    } 

    public function prices_post() {
        $data = $this->post();
        $id = $this->pricesmodel->insertMonitoring($data);
        if ($id) {
            $this->response([
                'message' => 'Successfully inserted monitoring',
                'id' => $id
            ], 200);
        }
        else  {
            $this->response($data, 404);
        }
    }

    public function reports_get() {
        $date = $this->input->get('monitoring_date'); // gusto niya ishow lang ang report ani na day
        $option = $this->input->get('option');
        if ($date) {
            $this->response('Trying to get report on '.$date, 200);
        }
        else if($option == 'all') {
            // meaning ishow tanan 
            $data = $this->pricesmodel->getPrevailingPriceReports();
            $this->response($data, 200);
        }

        $data = $this->reportingmodel->getSummarizedReport();
        if ($data) {
            $this->response($data, 200);

        }
        //  muning default if dili siya specific date, dili pud all, meaning latest report
        // $latest = $this->pricesmodel->getLatestReport(); 
        // $data = $this->pricesmodel->getSummaryReports($latest);
    }

}