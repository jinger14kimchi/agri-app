<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Trends extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('TrendModel', 'trends_m');
        $this->load->model('PricesModel', 'pricesmodel');
    }

    public function getTrends_get($product_id){
        $response = $this->trends_m->getCommodityTrend($product_id);
        if ($response) {
            return $this->response($response);
        }
    }

    public function trend_product_get() {
        $id = $this->input->get('id');
        $product_trend = $this->trends_m->getTrendByProduct($id);
        if ($product_trend) {
            $this->response($product_trend, 200); 
        }
        else {
            $this->response(['message' => 'Not found'], 404);
        }
    }

    public function product_get() {
        // get both prev_price from both markets
        // filter by product id and date range
        // http://localhost/agritrend-web/api/trends/product?id=1&start_date=2018-10-10&end_date=2019-01-20

        $start = $this->input->get('start_date');
        $end = $this->input->get('end_date');
        $prod_id = $this->input->get('id');
        $result = $this->trends_m->getTrendDateRange($start, $end, $prod_id);
        $this->response($result, 200);
    } 

    public function latest_get() {
        // get last date where monitoring is done
        // returns date and prev price both from carmen cogon
        // get param id if want specific
        $prod_id = $this->input->get('id');
        $res = $this->trends_m->getLatestPrevPrice($prod_id);
        if ($res) {
            $this->response($res, 200);
        } 
        else {
            $this->response(['message'=>'No data found for this product'], 404);
        }
    }


}
