<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Monitoring extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('MonitoringModel', 'monitormodel');
    }

    public function monitoring_get() {
        $id = $this->get('id');
        $market_id = $this->get('market_id');
        $date = $this->get('date');


        if ($id === NULL && $market_id === NULL) {
            $monitorings = $this->monitormodel->getMonitorings();
            if ($monitorings) {
                $this->response($monitorings, REST_Controller::HTTP_OK);
            }
        }

        else if ($market_id !== NULL) {
            if ($date !== NULL) {
                $monitoringByMarketByDate = $this->monitormodel->getMonitoringByMarketByDate($market_id, $date); 
                $this->response($monitoringByMarketByDate, REST_Controller::HTTP_OK);
            }
            else {
                $monitoredByMarket = $this->monitormodel->getMonitoringByMarket($market_id);
                if ($monitoredByMarket) {
                    $this->response($monitoredByMarket, REST_Controller::HTTP_OK);
                }
            }
        }

        else {        	
            $monitor = $this->monitormodel->getMonitoring($id);
            $this->response($monitor, 200);
        }
    }

    public function alldates_get() {
        $alldates = $this->monitormodel->getAllDates();
        if ($alldates) {
            $this->response($alldates, 200);
        }
        else {
            $this->response('', 404);
        }
    }

    public function reportdate_get() {
        $date = $this->input->get('date');
        $reportdate = $this->monitormodel->getReportDate($date);
        if ($reportdate) {
            $this->response($reportdate, 200);
        }
        else {
            $this->response('', 404);
        }

    }
}