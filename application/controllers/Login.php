<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$this->checkIfSessionActive();
		
	}

	public function checkIfSessionActive() {
		$sess_id = $this->session->userdata('user_id');
		echo $sess_id;

		if(!empty($sess_id)) {
			redirect(base_url().'pages/dashboard');
		}
		else {			
			$this->load->view('pages/login');
		}
	}
}
